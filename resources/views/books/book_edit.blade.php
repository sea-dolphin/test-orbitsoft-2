@extends('default')

@section('content')

    <div class="container-fluid">
        <div class="row">
            
            @include('left_menu');
            
            <div ng-controller="BookEditController">
            
                <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

                    <h2 class="sub-header">Редактирование книги</h2>
                    <div class="table-responsive">
                        <% book.name %>
                        <form name="formAddBook" novalidate>
                            <br>
                            <div class="input-group">
                                <span class="input-group-addon">Название</span>
                                <input name="name" type="text" class="form-control" ng-model="name" ng-required="true" ng-maxlength="150" value="123" ng-value="123">
                            </div>
                            <span ng-show="formAddBook.name.$error.required">Название обязательно.</span>
                            <br>
                            <div class="input-group">
                                <span class="input-group-addon">Автор</span>
                                <input name="author" type="text" class="form-control" ng-model="author" ng-required="true" ng-maxlength="100">
                            </div>
                            <span ng-show="formAddBook.author.$error.required">Поле Автор обязательно.</span>
                            <br>
                            <div class="input-group">
                                <span class="input-group-addon">Год издания</span>
                                <input name="year_of_publishing" type="number" ng-model="year_of_publishing">
                            </div>
                            <br>
                            <div class="form-group">
                                <label for="comment">Описание:</label>
                                <textarea name="description" class="form-control" rows="5" id="comment" ng-model="description" ng-required="true" ng-maxlength="2000"></textarea>
                            </div>
                            <span ng-show="formAddBook.description.$error.required">Поле Описание обязательно.</span>
                            <br><br>
                            <span class="btn btn-default btn-file">
                                Выбрать изображение <input type="file" name="myFile">
                            </span>
                            <br>
                            <br>
                            <button type="button" class="btn btn-success" ng-click="saveBook()">Сохранить</button>
                        </form>

                        <br>

                        <button type="button" class="btn btn-danger" ng-click="deleteBook()">Удалить</button>

                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@section('js_script')

    <script>
        
        myApp.controller("BookEditController", function($scope, $window, $http, $location){
            
            var aCurrentUrl = $location.absUrl().split("/");
            var id = aCurrentUrl[4];
            
            $http({
                method: 'GET',
                url: '/books/' + id + '/show'
            }).then(function successCallback(response) {
                    
                    $scope.book = response.data;
                    
                    $scope.name = $scope.book.name;
                    $scope.author = $scope.book.author;
                    $scope.year_of_publishing = $scope.book.year_of_publishing;
                    $scope.description = $scope.book.description;
                    
            }, function errorCallback(response) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
            
            //обновление
            $scope.saveBook = function (){
                
                if ($scope.formAddBook.$valid)
                {
                    $http({
                        method: 'PUT',
                        url: '/books/' + id,
                        data: {
                            name : $scope.name, 
                            author : $scope.author,
                            year_of_publishing : $scope.year_of_publishing,
                            description : $scope.description,
                        }
                    }).then(function successCallback(response) {
                            if (response.data.result == 1) $window.alert('Запись успешно обновлена');
                            
                    }, function errorCallback(response) {
                            // called asynchronously if an error occurs
                            // or server returns response with an error status.
                    });
                }
            };
            
            //удаление
            $scope.deleteBook = function (){
                
                $http({
                    method: 'DELETE',
                    url: '/books/' + id,
                }).then(function successCallback(response) {
                        if (response.data.result == 1) $window.alert('Запись удалена');
                        $window.location.href = '/books';
                }, function errorCallback(response) {
                        // called asynchronously if an error occurs
                        // or server returns response with an error status.
                });
                
            };
            
        });
        
    </script>

@stop