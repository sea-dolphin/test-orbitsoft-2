@extends('default')

@section('content')

<div class="container-fluid">
        <div class="row">
        
          @include('left_menu');
          
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
<!--          <h1 class="page-header">Dashboard</h1>-->

            <h2 class="sub-header">Список книг</h2>
            <div class="table-responsive">
              
                <a href="#" id="go"><div class="btn btn-success">Добавить книгу</div></a>
                <br>
              
                <div ng-controller="BooksListController">
                    
                    <br>
                    <form name="formBookSearch">
                        <div class="form-group">
                            <label for="usr">Строка поиска:</label>
                            <input type="text" name="search" class="form-control" id="usr" ng-model="search">
                            <label for="sel1">Сортировать по:</label>
                            <select class="form-control" id="sel1" name="filter_field" ng-model="filter_field">
                                <option value="name" selected="">Названию</option>
                                <option value="author">Автору</option>
                            </select>
                            <br>
                            <label class="radio-inline"><input type="radio" name="order" ng-model="order" value="asc">По возрастанию</label>
                            <label class="radio-inline"><input type="radio" name="order" ng-model="order" value="desc">По убыванию</label>
                            <br><br>
                            <button type="button" class="btn btn-primary" ng-click="searchBook()">Искать</button>
                        </div>
                    </form>
                
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Действие</th>
                                <th>Название</th>
                                <th>Автор</th>
                                <th>Год издания</th>
                                <th>Описание</th>
                                <th>Обложка</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="book in books">
                                <td><a href="/books/<%book.id%>/edit" title="редактировать"><span class="glyphicon glyphicon-pencil"></span></a></td>
                                <td><a href="/books/<%book.id%>"><% book.name %></a></td>
                                <td><% book.author %></td>
                                <td><% book.year_of_publishing %></td>
                                <td><% book.description %></td>
                                <td>Обложка</td>
                            </tr>
                        </tbody>
                    </table>
                    
                    <ul class="pagination" ng-repeat="current_page in paginations">
                        <li><a href="#" ng-click="goPage(current_page)"><% current_page %></a></li>
                    </ul>
                    
                        <!-- Модальное окно -->
                        <div id="modal_form">
                            <span id="modal_close">X</span>
                            <form name="formAddBook" novalidate>
                                <br>
                                <div class="input-group">
                                    <span class="input-group-addon">Название</span>
                                    <input name="name" type="text" class="form-control" ng-model="name" ng-required="true" ng-maxlength="150">
                                </div>
                                <span ng-show="formAddBook.name.$error.required">Название обязательно.</span>
                                <br>
                                <div class="input-group">
                                    <span class="input-group-addon">Автор</span>
                                    <input name="author" type="text" class="form-control" ng-model="author" ng-required="true" ng-maxlength="100">
                                </div>
                                <span ng-show="formAddBook.author.$error.required">Поле Автор обязательно.</span>
                                <br>
                                <div class="input-group">
                                    <span class="input-group-addon">Год издания</span>
                                    <input name="year_of_publishing" type="number" ng-model="year_of_publishing">
                                </div>
                                <br>
                                <div class="form-group">
                                    <label for="comment">Описание:</label>
                                    <textarea name="description" class="form-control" rows="5" id="comment" ng-model="description" ng-required="true" ng-maxlength="2000"></textarea>
                                </div>
                                <span ng-show="formAddBook.description.$error.required">Поле Описание обязательно.</span>
                                <br><br>
                                <span class="btn btn-default btn-file">
                                    Выбрать изображение <input type="file" name="myFile">
                                </span>
                                <br>
                                <br>
                                <button type="button" class="btn btn-success" ng-click="addBook()">Добавить книгу</button>
                            </form>
                        </div>
                        <div id="overlay"></div>
                    
                </div><!-- END BooksListController -->
                
          </div>
        </div>
      </div>
    </div>

@stop

@section('js_script')

    <script>
        myApp.controller("BooksListController", function($scope, $window, $http){
            
            $http({
                method: 'GET',
                url: '/books_list'
            }).then(function successCallback(response) {
                    
                    $scope.books = response.data.data;
                    myPagination(response);
                    
            }, function errorCallback(response) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
                    
            $scope.addBook = function (){
                
//                console.log($scope.name);
//                console.log($scope.formAddBook.$valid);
//                if ($scope.formAddBook.name.$valid)
//                {
//                    console.log('Yes');
//                }
//                else
//                {
//                    console.log('No');
//                }
                
                if ($scope.formAddBook.$valid)
                {
                    $http({
                        method: 'POST',
                        url: '/books',
                        data: {
                            name : $scope.name, 
                            author : $scope.author,
                            year_of_publishing : $scope.year_of_publishing,
                            description : $scope.description,
                        }
                    }).then(function successCallback(response) {
                            if (response.data.result == 1) $window.alert('Книга добавлена');
                            //$window.location.href = '/books';
                            
                    }, function errorCallback(response) {
                            // called asynchronously if an error occurs
                            // or server returns response with an error status.
                    });
                }
            };
            
            $scope.searchBook = function (){
                
                $http({
                    method: 'GET',
                    url: '/books_list',
                    params: {
                        search : $scope.search, 
                        filter_field : $scope.filter_field,
                        order : $scope.order
                    }
                }).then(function successCallback(response) {
                    $scope.books = response.data.data;
                    myPagination(response);
                    
                }, function errorCallback(response) {
                        // called asynchronously if an error occurs
                        // or server returns response with an error status.
                });
            };
            
            //пагинация
            $scope.goPage = function (current_page){
                
                $http({
                    method: 'GET',
                    url: '/books_list',
                    params: {
                        search : $scope.search, 
                        filter_field : $scope.filter_field,
                        order : $scope.order,
                        page : current_page,
                    }
                }).then(function successCallback(response) {

                        $scope.books = response.data.data;

                        myPagination(response);

                }, function errorCallback(response) {
                        
                });
            }
            
            //пагинация
            function myPagination(response)
            {
                $scope.paginations = [];
                for(var i = 1; i <= response.data.last_page; i++)
                {
                    $scope.paginations.push(i);
                }
            }
            
        });
        
        
        
    </script>

@stop