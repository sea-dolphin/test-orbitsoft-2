@extends('default')

@section('content')

    <div class="container-fluid">
        <div class="row">
            
            @include('left_menu');
            
            <div ng-controller="BookDetailController">
                
                <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

                    <h2 class="sub-header">Детальная информация о книге</h2>
                    <div class="table-responsive">

                        <p>
                            <strong>Название:</strong><br>
                            <% book.name %>
                        </p>
                        <p>
                            <strong>Автор:</strong><br>
                            <% book.author %>
                        </p>
                        <p>
                            <strong>Год издания:</strong><br>
                            <% book.year_of_publishing %>
                        </p>
                        <p>
                            <strong>Описание:</strong><br>
                            <% book.description %>
                        </p>
                        <p>
                            <strong>Обложка:</strong><br>
                            
<!--                                <img src="/download_user/" width="400">-->
                            
                        </p>

                    </div>
                </div>
            </div>
            
        </div>
    </div>

@stop

@section('js_script')

    <script>
        
        myApp.controller("BookDetailController", function($scope, $window, $http, $location){
            
            var aCurrentUrl = $location.absUrl().split("/");
            var id = aCurrentUrl[4];
            
            $http({
                method: 'GET',
                url: '/books/' + id + '/show'
            }).then(function successCallback(response) {
                    // this callback will be called asynchronously
                    // when the response is available
                    //$window.alert('myTest');
                    
                    $scope.book = response.data;
            }, function errorCallback(response) {
                    // called asynchronously if an error occurs
                    // or server returns response with an error status.
            });
            
        });
        
    </script>

@stop