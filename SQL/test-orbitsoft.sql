-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Апр 18 2017 г., 00:19
-- Версия сервера: 5.7.16
-- Версия PHP: 7.1.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `test-orbitsoft`
--

-- --------------------------------------------------------

--
-- Структура таблицы `books`
--

CREATE TABLE `books` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `author` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `year_of_publishing` int(11) DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `books`
--

INSERT INTO `books` (`id`, `name`, `author`, `year_of_publishing`, `description`, `image`, `created_at`, `updated_at`) VALUES
(1, 'PHP 7 в подлиннике', 'Дмитрий Котеров, Игорь Симдянов', 2016, 'Подробное описание книги', '1492457450.jpg', '2017-04-15 04:00:00', '2017-04-17 19:24:41'),
(2, 'Zend Framework 2.0. Разработка веб-приложений', 'Шасанкар Кришна', 2014, 'Zend Framework 2 представляет собой последнее обновление широко известного фреймворка Zend Framework. Эта версия значительно упростила процесс создания сложных веб-приложений, сведя к минимуму усилия разработчиков благодаря наличию готовых к использованию компонентов. Zend Framework 2 — это многофункциональный масштабируемый фреймворк для разработки веб-приложений.\r\nДанная книга послужит для вас руководством по созданию мощных веб-приложений средствами Zend Framework 2. В ней рассматриваются все аспекты создания приложений на основе Zend Framework, начиная с установки и конфигурирования среды разработки, а имеющиеся упражнения позволят вам с легкостью разобраться в возможностях ZF и воспользоваться ими для создания собственных приложений.', NULL, '2017-04-14 21:00:00', '2017-04-17 19:53:24'),
(3, 'NODE.JS Путеводитель по технологии', 'Кирилл Сухов', 2015, 'За последние несколько лет платформа Node.js стремительно повысила свой\r\nстатус от экспериментальной технологии до основы для серьезных промышленных\r\nпроектов. Тысячи программистов оценили возможность построения достаточно\r\nсложных, высоко нагруженных приложений на простом, элегантном и, самое\r\nглавное, легковесном механизме.\r\nВсе эти скучные слова правдивы, но на самом деле не это главное. Прежде\r\nвсего Node.js – это совершенно увлекательная и захватывающая вещь, с которой\r\nпо-настоящему интересно работать!', '', '2017-04-14 21:00:00', '2017-04-17 17:13:08'),
(4, 'Разработка через тестирование', 'Кент Бек', 2003, 'Гибкий и понятный код', NULL, '2017-04-14 21:00:00', '2017-04-14 21:00:00'),
(6, 'Malibri', 'Автор', 2017, 'fgdhfgh  hdfhdfgh', '1492445635.jpg', '2017-04-17 16:12:32', '2017-04-17 16:12:32'),
(7, 'Malibri', 'Автор', 2017, 'tetytert', '1492459428.jpg', '2017-04-17 19:52:14', '2017-04-17 19:52:14');

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_04_14_163100_create_books_table', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `books`
--
ALTER TABLE `books`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT для таблицы `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
