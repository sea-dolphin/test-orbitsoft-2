<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::resource('/books', 'BooksController');

Route::get('/books', 'BooksController@default_page');
Route::get('/books_list', 'BooksController@index');
Route::post('/books', 'BooksController@store');
Route::get('/books/{id}', 'BooksController@show_page');
Route::get('/books/{id}/show', 'BooksController@show');
Route::get('/books/{id}/edit', 'BooksController@edit_page');
Route::put('/books/{id}', 'BooksController@update');
Route::delete('/books/{id}', 'BooksController@destroy');

Route::get('/test', 'TestController@index');
