<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

class ImagesController extends Controller
{
    private $aImageMime = ["image/pjpeg", "image/jpeg", "image/x-png", "image/png", "image/gif"];
    private $aImageExtension = ["jpg", "jpeg", "gif", "png"];
    
    public function loadImages(Request $request)
    {
        if ($request->file('file'))
        {
            $nFileSize = $request->file->getClientSize() / 1024;
            
            if ($nFileSize <= 500)
            {
                if (in_array(strtolower($request->file->getMimeType()), $this->aImageMime) && in_array(strtolower($request->file->getClientOriginalExtension()), $this->aImageExtension))
                {
                    $strNameImage = (time() - rand(100, 999) + rand(100, 999)).'.'.$request->file->getClientOriginalExtension();
                    $oImage = Image::make($request->file('file'));
                    
                    //в задании ничего не сказано про разрешение картинки, но решил уменьшить размер
                    $oImage->resize(800, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    $oImage->save('download_user/'.$strNameImage);
                    
                    return $strNameImage;
                }
            }
            else return '';
        }
        else return '';
        
    }
    
    public function deleteImage($strNameImage)
    {
        @unlink('download_user/'.$strNameImage);
        return true;
    }
}
