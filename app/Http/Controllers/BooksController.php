<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\ImagesController;

use App\Models\Book;

class BooksController extends Controller
{
    private $pageCount = 3;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function default_page(Request $request)
    {
        return view('books.books');
    }
    
    public function index(Request $request)
    {
        //формирую строку поиска
        $strQuery = ' 1 = ? ';
        $aQueryValues[] = 1;
        
        //упорядочивание по умолчанию
        $strField = 'id';
        $strOrder = 'DESC';
        
        if (!empty($request->input('search')))
        {
            $strQuery .= ' AND (name LIKE ? OR author LIKE ?) ';
            $aQueryValues[] = '%'.$request->input('search').'%';
            $aQueryValues[] = '%'.$request->input('search').'%';
        }
        
        if (!empty($request->input('filter_field')) && !empty($request->input('order')))
        {
            $strField = $request->input('filter_field');
            $strOrder = $request->input('order');
        }
        
        //формирование строки для пагинации
        $aPaginationParametr = [
            'search' => empty($request->input('search')) ? '' : $request->input('search'),
            'filter_field' => empty($request->input('filter_field')) ? '' : $request->input('filter_field'), 
            'order' => empty($request->input('order')) ? '' : $request->input('order'),
        ];
        
        $jsonData = Book::whereRaw($strQuery, $aQueryValues)
                ->orderBy($strField, $strOrder)
                ->paginate($this->pageCount)
                ->toJson();
        
        print $jsonData;
        //return view('books.books', ['aData' => $jsonData, 'aPaginationParametr' => $aPaginationParametr]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('books.book_add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //в реальном проекте валидацию нужно вынести в отдельный метод
        //dd($request->input(), $request->file());
        $this->validate($request, [
            'name' => 'required|max:150',
            'author' => 'required|max:100',
            'year_of_publishing' => 'numeric',
            'description' => 'required|max:2000',
        ]);
        
        //$oImagesController = new ImagesController();
        //$strNameImage = $oImagesController->loadImages($request);
        
        $aData = [
            'name' => $request->input('name'),
            'author' => $request->input('author'),
            'year_of_publishing' => $request->input('year_of_publishing'),
            'description' => $request->input('description'),
            //'image' => $strNameImage,
        ];
        
        if (Book::create($aData))
        {
            print json_encode(['result' => 1]);
        }
        else
        {
            print json_encode(['result' => 0]);
        }
        
        //return redirect('/books');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show_page($id)
    {
        return view('books.book_detail');
    }
    
    public function show(Book $oBookModel, $id)
    {
        $jsonData = $oBookModel->getDetailItem($id)->toJson();
        print $jsonData;
        //return view('books.book_detail', ['oData' => $oData]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit_page($id)
    {
        return view('books.book_edit');
    }
    
    public function edit(Book $oBookModel, $id)
    {
        $oData = $oBookModel->getDetailItem($id);
        return view('books.book_edit', ['oData' => $oData]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id, Book $oBookModel)
    {
        //в реальном проекте валидацию нужно вынести в отдельный метод
        
        $this->validate($request, [
            'name' => 'required|max:150',
            'author' => 'required|max:100',
            'year_of_publishing' => 'numeric',
            'description' => 'required|max:2000',
        ]);
        
        $oData = $oBookModel->getDetailItem($id);
        
        //$oImagesController = new ImagesController();
        //$strNameImage = $oImagesController->loadImages($request);
        
        //в случае выборна новой обложки, старую удаляю
//        if (!empty($strNameImage))
//        {
//            $oImagesController->deleteImage($oData->image);
//        }
        
        $aData = [
            'name' => $request->input('name'),
            'author' => $request->input('author'),
            'year_of_publishing' => $request->input('year_of_publishing'),
            'description' => $request->input('description'),
            'image' => empty($strNameImage) ? $oData->image : $strNameImage,
        ];
        
        if (Book::where('id', '=', $id)->update($aData))
        {
            print json_encode(['result' => 1]);
        }
        else
        {
            print json_encode(['result' => 0]);
        }
        
        return;
        //return redirect('/books/'.$id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        Book::where('id', '=', $id)->delete();
        //Что бы не делать лишний запрос к базе, получаю имя картинки для удаления из скрытого поля формы
        if (!empty($request->input('image_for_delete')))
        {
            //здесь можно воспользоваться $oImagesController->deleteImage()
            @unlink('download_user/'.$request->input('image_for_delete'));
        }
        print json_encode(['result' => 1]);
        //return redirect('/books/');
    }
    
    
}
