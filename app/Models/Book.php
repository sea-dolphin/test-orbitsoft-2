<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $table = 'books';
    protected $guarded = [];
    
    public function scopeDetail($query, $id)
    {
        $query->where('id', '=', $id);
    }
    
    public function getDetailItem($id)
    {
        $oResult = $this->detail($id)->first();
        return $oResult;
    }
}
